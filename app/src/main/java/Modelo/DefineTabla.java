package Modelo;

public class DefineTabla {

    //Constructor vacio
    public DefineTabla(){}

    //Definicion de la tabla
    //Se crea un clase abstracta dentro de definie tabla
    public static abstract class Usuarios{
        //Variables necesarias para la creación de la tabla
        public static  final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NOMBREUSUARIO = "nombreUsuario";
        public static final String COLUMN_NAME_CORREO = "correo";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

    //Objeto para los registros
    public static String[] REGISTROS = new String[]{
            Usuarios.COLUMN_NAME_ID,
            Usuarios.COLUMN_NAME_NOMBREUSUARIO,
            Usuarios.COLUMN_NAME_CORREO,
            Usuarios.COLUMN_NAME_PASSWORD
    };


}

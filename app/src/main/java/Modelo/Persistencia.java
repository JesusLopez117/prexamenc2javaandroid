package Modelo;

import com.example.prexamenc2java.Usuario;

public interface Persistencia {
    //Funciones necesarias
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuario usuario);

}

package Modelo;

import android.database.Cursor;

import com.example.prexamenc2java.Usuario;

import java.util.ArrayList;

public interface Proyeccion {
    //Funciones necesarias
    public boolean obtenerUser(String correo);
    public boolean getUsuario(String nombre, String password);
    public ArrayList<Usuario> allUsuarios();
    public Usuario readUsuario(Cursor cursor);

}

package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.prexamenc2java.Usuario;

import java.util.ArrayList;

public class UsuarioDb implements Persistencia, Proyeccion{
    //Variables necesarias
    private Context context;
    private UsuarioDbHelper helper;
    private SQLiteDatabase db;

    //Constructores de parametros
    public UsuarioDb(Context context, UsuarioDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public UsuarioDb(Context context){
        this.context = context;
        this.helper = new UsuarioDbHelper(this.context);
    }

    @Override
    public void openDataBase(){ db = helper.getWritableDatabase(); }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertUsuario(Usuario usuario) {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Usuarios.COLUMN_NAME_NOMBREUSUARIO, usuario.getNombre());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_PASSWORD, usuario.getPassword());

        this.openDataBase();
        long num = db.insert(DefineTabla.Usuarios.TABLE_NAME, null, values);
        Log.d("Agregar", "Se inserto el usuario " + num);

        return num;
    }


    @Override
    public boolean obtenerUser(String correo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.REGISTROS,
                DefineTabla.Usuarios.COLUMN_NAME_CORREO + " = ? ",
                new String[] {correo},
                null, null, null);

        return cursor.moveToFirst();

        //Regresarr true si ya existe un usuario con ese correo
    }

    @Override
    public boolean getUsuario(String nombre, String password) {

        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.REGISTROS,
                DefineTabla.Usuarios.COLUMN_NAME_CORREO + " = ? AND " + DefineTabla.Usuarios.COLUMN_NAME_PASSWORD + " = ?",
                new String[] {nombre, password},
                null, null, null);

        return cursor.moveToFirst();

        //Regresara true si encuentra algun usuario con la contraseña y el correo ingresado.
    }

    @Override
    public ArrayList<Usuario> allUsuarios() {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.REGISTROS,
                null, null, null, null, null);

        ArrayList<Usuario> usuarios = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Usuario usuario = readUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }

        cursor.close();
        return usuarios;
    }

    @Override
    public Usuario readUsuario(Cursor cursor) {
        Usuario usuario = new Usuario();

        usuario.setIdUsuario(cursor.getInt(0));
        usuario.setNombre(cursor.getString(1));
        usuario.setCorreo(cursor.getString(2));
        usuario.setPassword(cursor.getString(3));

        return usuario;
    }
}

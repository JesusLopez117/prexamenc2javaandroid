package com.example.prexamenc2java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.UsuarioDb;

public class MainActivity extends AppCompatActivity {
    //Variables de entrada
    private EditText txtCorreo;
    private EditText txtPassword;

    //Variables de tipo texto
    private TextView lblEmpresas;

    //Varibles de tipo boton
    private Button btnSalir;
    private Button btnRegistrar;
    private Button btnIngresar;

    //Variable de la base de datos
    static UsuarioDb usuarioDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        usuarioDb = new UsuarioDb(getApplicationContext());
        usuarioDb.openDataBase();
        
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar();
            }
        });
        
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });

        
    }

    private void salir() {
        //Construción del dialogo de alerta
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage(" ¿Desea salir? ");

        //Confirmación
        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        //Negación
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();

    }

    private void registrar() {
        Intent intent = new Intent(MainActivity.this, registroActivity.class);
        startActivity(intent);
    }

    private void ingresar() {
        String correo = txtCorreo.getText().toString();
        String password = txtPassword.getText().toString();

        usuarioDb = new UsuarioDb(getApplicationContext());

        boolean siRegsitrado = usuarioDb.getUsuario(correo, password);
        usuarioDb.openDataBase();

        if(siRegsitrado){
            txtCorreo.setText("");
            txtPassword.setText("");
            Intent intent = new Intent(MainActivity.this, listActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Correo o Password incorrectos", Toast.LENGTH_LONG).show();
        }
    }

    //Función para inciar los componentes
    private void iniciarComponentes(){
        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtPassword);
        lblEmpresas = findViewById(R.id.lblEmpresas);
        btnSalir = findViewById(R.id.btnSalir);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnIngresar = findViewById(R.id.btnIngresar);
    }
}
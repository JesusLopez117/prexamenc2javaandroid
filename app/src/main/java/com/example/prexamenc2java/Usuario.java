package com.example.prexamenc2java;

public class Usuario {
    //Atributos de un usuario
    private int idUsuario;
    private String nombre;
    private String correo;
    private String password;

    //Constructor vacio
    public Usuario(){
        this.idUsuario = 0;
        this.nombre = "";
        this.correo = "";
        this.password = "";
    }

    //Constructor de parametros
    public Usuario(int id, String nombre, String correo, String password){
        this.idUsuario = id;
        this.nombre = nombre;
        this.correo = correo;
        this.password = password;
    }

    //Encapsulamiento


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

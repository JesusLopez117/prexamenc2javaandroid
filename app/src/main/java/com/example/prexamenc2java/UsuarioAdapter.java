package com.example.prexamenc2java;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UsuarioAdapter extends BaseAdapter {
    //Variables necesarias
    int gruopId;
    Context context;
    List<Usuario> lista;
    LayoutInflater inflater;

    //Constructor
    public UsuarioAdapter(Context context, List<Usuario> lista){
        this.lista = lista;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //Funciones
    public View getView(int posicion, View convertView, ViewGroup parent){

        Usuario c = lista.get(posicion);

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.itemusuario, null);
        }

        TextView txtNombre = (TextView) convertView.findViewById(R.id.lblNombre);
        txtNombre.setText(lista.get(posicion).getNombre());

        TextView txtCorreo = (TextView) convertView.findViewById(R.id.lblCorreo);
        txtCorreo.setText(lista.get(posicion).getCorreo());

        return convertView;
    }

}

package com.example.prexamenc2java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Modelo.UsuarioDb;

public class listActivity extends AppCompatActivity {
    //Variable de tipo boton
    private Button btnRegresar;

    //Variables de muestra de texto
    private ListView lstUsurios;
    private TextView lblEmpresas;
    private TextView lblUsuarios;

    //Variable para el adaptador
    private UsuarioAdapter miAdaptador;


    //Variable para el acceso a la base de datos
    static UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        iniciarComponentes();

        //Codigo donde se extraen los datos de la base de datos
        usuarioDb = new UsuarioDb(getApplicationContext());
        List<Usuario> lista = usuarioDb.allUsuarios();
        usuarioDb.openDataBase();

        miAdaptador = new UsuarioAdapter(listActivity.this, lista);
        lstUsurios.setAdapter(miAdaptador);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }

    private void regresar() {
        //Construción del dialogo de alerta
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage(" ¿Desea regresar a la pagina principal? ");

        //Confirmación
        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        //Negación
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }

    private void iniciarComponentes() {
        btnRegresar = findViewById(R.id.btnRegresar);
        lstUsurios = findViewById(R.id.lstUsuarios);
        lblEmpresas = findViewById(R.id.lblEmpresas);
        lblUsuarios = findViewById(R.id.lblUsuarios);
    }
}
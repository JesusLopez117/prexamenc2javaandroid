package com.example.prexamenc2java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.UsuarioDb;

public class registroActivity extends AppCompatActivity {
    //Variables de tipo entrada
    private EditText txtNombre;
    private EditText txtCorreo;
    private EditText txtPassword;
    private EditText txtPassword2;

    //Varibales de texto muestra
    private TextView lblEmpresas;
    private TextView lblRegistro;

    //Varibles de tipo boton
    private Button btnRegistrar;
    private Button btnRegresar;

    //Varible de item
    private Usuario usuario;

    //Variables relacionadas con la base de datos
    private UsuarioDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtNombre = findViewById(R.id.txtNombre);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtPassword = findViewById(R.id.txtPassword);
        txtPassword2 = findViewById(R.id.txtPassword2);
        lblEmpresas = findViewById(R.id.lblEmpresas);
        lblRegistro = findViewById(R.id.lblRegistro);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
        
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar();
            }
        });
    }

    private void registrar() {
        //Agregar un nuevo usuario
        usuario = new Usuario();
        usuario.setNombre(txtNombre.getText().toString());
        usuario.setCorreo(txtCorreo.getText().toString());
        usuario.setPassword(txtPassword.getText().toString());

        if(validar()){
            if(validarCorreoExis()){
                Toast.makeText(getApplicationContext(), " Correo ya existente ", Toast.LENGTH_SHORT).show();
            }else{
                usuarioDb = new UsuarioDb(getApplicationContext());
                //Inserta en la tabla
                usuarioDb.insertUsuario(usuario);
                Toast.makeText(getApplicationContext(), "Se inserto un usuario ",Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Falto capturar datos ", Toast.LENGTH_SHORT).show();
        }

    }

    private void regresar() {
        //Construción del dialogo de alerta
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Empresas Industriales de México");
        dialogo.setMessage(" ¿Desea regresar a la pagina principal? ");

        //Confirmación
        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        //Negación
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }

    //Función para validar los campos de texto
    private boolean validar(){

        if (txtNombre.getText().toString().equals("")) return false;
        if (txtCorreo.getText().toString().equals("")) return false;
        if (txtPassword.getText().toString().equals("")) return false;
        if(txtPassword2.getText().toString().equals(txtPassword.getText().toString())) return true;

        return false;
    }

    private boolean validarCorreoExis(){
        usuarioDb = new UsuarioDb(getApplicationContext());

        boolean correoExistente = usuarioDb.obtenerUser(txtCorreo.getText().toString());
        usuarioDb.openDataBase();

        return correoExistente;
    }
}